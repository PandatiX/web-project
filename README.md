# NodeJS TODO List

This project is a simple NodeJS web app to manage a TODO List...
Which could be pretty usefull by the end of the second semester
at the ENSIBS !

It aims to be able to save data in a database, feature drag&drop and
tasks ordering.

This project is built by:
 - Lucas TESSON - e2004675: dev env (vagrant setup) + back-end (RESTful API + functionnal tests) ;
 - Lucas VIDELAINE - e2004769: front-end (NodeJS views using the back-end.

## Run the project

To start the project, you have to do the following.

```bash
node index.js
```

You should see a successful connection test to the database.

Nevertheless, you may install things as NodeJS, NPM dependencies...etc.
If so, refer to the `Vagrantfile` at the project root.

If nothing works, please file an issue on the [GitLab Project page > issues](https://gitlab.com/PandatiX/web-project/-/issues).

## Run the API functionnal tests (with RobotFramework)

To run those, run the following, with the http endpoint to test in the test file.

```bash
robot robot/tests
```

## TODO

As all good TODO List project, here is our current TODO List for it:
 - [X] Setup the project ;
 - [X] Connect to the database ;
 - [X] RESTful API ;
 - [X] Automated RESTful API functional tests (robotframework) ;
 - [ ] Shielded RESTful API (issues pointed by the functional tests) ;
 - [ ] Views ;
 - [ ] Drag&Drop ;
 - [ ] Ordering.

As you can see, the view part of the project should be built for it to works.
