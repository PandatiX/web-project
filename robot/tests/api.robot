*** Settings ***
Documentation  Test suite for the API routes.
Library        Browser

Suite Setup     New Page  http://172.28.128.7:8080
Suite Teardown  Close Page

*** Test Cases ***
# GET tests
GET /tasks 200
    [Documentation]  This test ensures the GET /tasks route works
    ...              as expected.

    &{response}=     HTTP                /tasks  GET
    Should Be Equal  ${response.status}  ${200}

# POST tests
POST /tasks 201
    [Documentation]  This test ensures the POST /tasks route works
    ...              with a valid json body.

    &{response}=     HTTP                /tasks  POST  {"libelle":"test"}
    Should Be Equal  ${response.status}  ${201}

POST /tasks 400 (invalid)
    [Documentation]  This test ensures the POST /tasks route works
    ...              with an invalid json body.

    &{response}=     HTTP                /tasks  POST  {"invalid":1}
    Should Be Equal  ${response.status}  ${400}

POST /tasks 400 (malformed)
    [Documentation]  This test ensures the POST /tasks route works
    ...              with a malformed json body.

    &{response}=     HTTP                /tasks  POST  {Not a valid json
    Should Be Equal  ${response.status}  ${400}

# PUT tests
PUT /tasks 200
    [Documentation]  This test ensures the PUT /tasks route works
    ...              with a valid json body.

    &{response}=     HTTP                /tasks  PUT  {"id":1,"status":"A_FAIRE"}
    Should Be Equal  ${response.status}  ${200}

PUT /tasks 400 (invalid json body)
    [Documentation]  This test ensures the PUT /tasks route works
    ...              with an invalid json body.
    ...              It should respond a 400 because the update can't
    ...              work on null.

    &{response}=     HTTP                /tasks  PUT  {"invalid":1}
    Should Be Equal  ${response.status}  ${400}

PUT /tasks 400 (null status)
    [Documentation]  This test ensures the PUT /tasks route works
    ...              with an invalid json body.
    ...              It should respond a 400 because the update will
    ...              do nothing on a valid id without a status.

    &{response}=     HTTP                /tasks  PUT  {"id":1}
    Should Be Equal  ${response.status}  ${400}

PUT /tasks 400 (invalid id)
    [Documentation]  This test ensures the PUT /tasks route works
    ...              with a valid json body.
    ...              This call should give a 400 response code because
    ...              you can't do an action on an invalid id which was
    ...              given by a user.

    &{response}=     HTTP                /tasks  PUT  {"id":-1,"status":"A_FAIRE"}
    Should Be Equal  ${response.status}  ${400}

PUT /tasks 400 (malformed)
    [Documentation]  This test ensures the PUT /tasks route works
    ...              with a malformed json body.

    &{response}=     HTTP                /tasks  PUT  {Not a valid json
    Should Be Equal  ${response.status}  ${400}

PUT /tasks 500
    [Documentation]  This test ensures the PUT /tasks route works
    ...              with an unknown status.

    &{response}=     HTTP                /tasks  PUT  {"id":1,"status":"unknown"}
    Should Be Equal  ${response.status}  ${500}

# DELETE tests
DELETE /tasks 200
    [Documentation]  This test ensures the DELETE /tasks route works
    ...              with a valid json body.

    &{response}=     HTTP                /tasks  DELETE  {"id":1}
    Should Be Equal  ${response.status}  ${200}

DELETE /tasks 400 (invalid json body)
    [Documentation]  This test ensures the DELETE /tasks route works
    ...              with an invalid json body.
    ...              It should respond a 400 because the delete can't
    ...              work on null.

    &{response}=     HTTP                /tasks  DELETE  {"invalid":1}
    Should Be Equal  ${response.status}  ${400}

DELETE /tasks 400 (invalid id)
    [Documentation]  This test ensures the DELETE /tasks route works
    ...              with a valid json body.
    ...              This call should give a 400 response code because
    ...              you can't do an action on an invalid id which was
    ...              given by a user.

    &{response}=     HTTP                /tasks  DELETE  {"id":-1}
    Should Be Equal  ${response.status}  ${400}
