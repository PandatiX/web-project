// Dependencies
const express = require("express");
const path = require("path");
const db = require("./db.js")

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

/***************/
/***** API *****/
/***************/

// GET /tasks
// Get all the tasks
app.get("/tasks", (req, res) => {
    db.then( pool =>
        pool.query("SELECT id, libelle, status FROM taches")
    ).then( results => {
        res.status(200);
        res.json(results);
    }).catch( _ => {
        res.status(500);
        res.json({message:"Failed to get the tasks."})
    });
});

// POST /tasks
// Create a task according to a libelle
//
// Expect json content as follows:
// {
//     "libelle": "xxx",
// }
app.post("/tasks", (req, res) => {
    db.then( pool => 
        pool.query("INSERT INTO taches(libelle, status) VALUES(?, 'A_FAIRE')", [req.body.libelle])
    ).then( _ => {
        res.status(201);
        res.json(); // send an empty content
    }).catch( err => {
        var msg;
        if (err.code === 'ER_BAD_NULL_ERROR') {
            res.status(400);
            msg = 'Expected json content as {"libelle":"xxx"}';
        } else {
            res.status(500);
            msg = 'Unhandled error';
        }
        res.json({message: msg});
    });
});

// PUT /tasks
// Changes a task status given the new status and its ID
//
// Expect json content as follows:
// {
//     "id": x,
//     "status": "x",
// }
app.put("/tasks", (req, res) => {
    db.then( pool =>
        pool.query("UPDATE taches SET status=? WHERE id =?", [req.body.status, req.body.id])
    ).then( _ => {
        res.status(200);
        res.json(); // send an empty content
    }).catch( err => {
        var msg;
        if (err.code === 'ER_BAD_NULL_ERROR') {
            res.status(400);
            msg = 'Expected json content as {"id":x,"status":"xxx"}';
        } else {
            res.status(500);
            msg = 'Unhandled error';
        }
        res.json({message: msg});
    });
});

// DELETE /tasks
// Delete a task given its ID
//
// Expect json content as follows:
// {
//     "id": x,
// }
app.delete("/tasks", (req, res) => {
    db.then( pool =>
        pool.query("DELETE FROM taches WHERE id=?", [req.body.id])
    ).then( _ => {
        res.status(200);
        res.json(); // send an empty content
    }).catch( err => {
        var msg;
        if (err.code === 'ER_BAD_NULL_ERROR') {
            res.status(400);
            msg = 'Expected json content as {"id":x}';
        } else {
            res.status(500);
            msg = 'Unhandled error';
        }
        res.json({message: msg});
    });
});

/*****************/
/***** FRONT *****/
/*****************/
// GET /
app.get("/", (_, res) => {
    res.status(200);
    res.sendFile(path.join(__dirname, "/views/index.html"));
});

// Server entrypoint
PORT = 8080
app.listen(PORT, () => {
    console.log("Server is running on port " + PORT);
});
